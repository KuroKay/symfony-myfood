<?php

namespace App\DataFixtures;

use App\Entity\Recipe;
use App\Entity\Ingredient;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class IngredientFixtures extends Fixture
{
  public function load(ObjectManager $manager)
  {
    // $product = new Product();
    // $manager->persist($product);

    // Met FR_fr pour avoir des données en français mais on peut lui dire ENG_eng
    $faker = \Faker\Factory::create('fr_FR');

    //Création de 3 catégory fake
    for ($i = 1; $i <= 3; $i++) {
      $ingredient = new Ingredient();
      $ingredient->setTitle($faker->sentence())
        ->setImage($faker->imageUrl())
        ->setMonth($faker->sentence());

      $manager->persist($ingredient);
    }

    //Balance la requête sql
    $manager->flush();
  }
}
