<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Recipe;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RecipeFixtures extends Fixture
{
  public function load(ObjectManager $manager)
  {
    // $product = new Product();
    // $manager->persist($product);

    // Met FR_fr pour avoir des données en français mais on peut lui dire ENG_eng
    $faker = \Faker\Factory::create('fr_FR');

    //Création de 3 catégory fake
    for ($i = 1; $i <= 3; $i++) {
      $category = new Category();
      $category->setTitle($faker->sentence())
        ->setDescription($faker->paragraph());

      $manager->persist($category);

      //Créer entre 4 et 6 articles
      //Attention ne pas réutiliser la variable i pour incrémenter
      for ($j = 0; $j <= mt_rand(4, 6); $j++) {
        $recipe = new Recipe();

        //Pour éviter que le tableau de paragraphes pose problème
        $content = '<p>' . join($faker->paragraphs(5), '</p><p>') . '</p>';

        $recipe->setTitle($faker->sentence())
          ->setText($content)
          ->setImage($faker->imageUrl())
          ->setCreatedAt($faker->dateTimeBetween('-6months'));

        //Demande a notre manager de le faire persister dans le temps
        //Preparation a le faire persister
        $manager->persist($recipe);

        //Créer entre 4 et 6 commentaires dans la recette
        for ($k = 0; $k <= mt_rand(4, 6); $k++) {
          $comment = new Comment();

          $content = '<p>' . join($faker->paragraphs(2), '</p><p>') . '</p>';

          //Va cherche la difference entre les 2 dates
          $now = new \DateTime();
          $interval = $now->diff($recipe->getCreatedAt());
          $days = $interval->days;
          $min = '-' . $days . ' days'; // -100 days

          $comment->setAuthor($faker->name)
            ->setContent($content)
            ->setCreatedAt($faker->dateTimeBetween($min))
            ->setRecipe($recipe);

          $manager->persist($comment);
        }
      }
    }

    //Balance la requête sql
    $manager->flush();
  }
}
