<?php

namespace App\Controller;

use App\Entity\Ingredient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IngredientController extends AbstractController
{
  /**
   * @Route("/ingredient", name="ingredient")
   */
  public function index()
  {
    //Va demander à  doctrine de discuter avec le repo de l'entity Recipe
    //Pour chopper les recipes
    $repo = $this->getDoctrine()->getRepository(Ingredient::class);

    //Puis on demande de les trouver tous
    $ingredients = $repo->findAll();

    return $this->render('ingredient/index.html.twig', [
      'controller_name' => 'IngredientController',
      //creer un variable pour twig afin de récuperer les recipes
      'ingredients' => $ingredients
    ]);
  }
}
