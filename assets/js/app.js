require('slick-carousel');
require('fullpage.js');
const $script = require('./vendors/script.min.js');

import fullpage from "fullpage.js";

// require jQuery normally
const $ = require('jquery');

$(document).ready(function () {
  $('.slick-recipe').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    rows: 2,
    centerMode: true,
    variableWidth: true,
    prevArrow: '<button class="slide-arrow prev-arrow chevron-left "></button>',
    nextArrow: '<button class="slide-arrow next-arrow chevron-left "></button>',
  })
  new fullpage("#fullpage", {
    licenseKey: "OPEN-SOURCE-GPLV3-LICENSE",
    verticalCentered: true,
    controlArrows: false,
    scrollBar: true,
    dots: false,
  });

  $('.next-arrow').click(function () {
    $(this).prev().slick('slickNext');
  });
  $('.prev-arrow').click(function () {
    $(this).next().slick('slickPrev');
  });

  //Navigation burger menu
  $('.burger-menu').click(function () {
    $(".cp-nav__list").toggleClass("open");
  });

})

//map 
let $map = document.querySelector('#map')
class GoogleMap {
  /**
   * Charge la carte sur un element
   * @param {HTMLElement} element 
   */
  load(element) {
    $script('https://maps.googleapis.com/maps/api/js?key=AIzaSyBVTQyyEoulOCyR-R3AMCzkG8DlJwe6Eso=&callback=initMap', function () {
      var uluru = { lat: 131.044, lng: -25.363 };
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 4,
        center: uluru
      });
    })
  }
}

if (map !== null) {
  let map = new GoogleMap()
  map.load($map)
}
