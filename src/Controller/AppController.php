<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Controller\RecipeController;
use App\Entity\Recipe;

class AppController extends AbstractController
{
  /**
   * @Route("/app", name="app")
   */
  public function index()
  {

    return $this->render('app/index.html.twig', [
      'controller_name' => 'AppController',
    ]);
  }


  /**
   * @Route("/", name="app_homepage")
   */
  public function home()
  {

    //Va demander à  doctrine de discuter avec le repo de l'entity Recipe
    //Pour chopper les recipes
    $repo = $this->getDoctrine()->getRepository(Recipe::class);

    //Puis on demande de les trouver tous
    $recipes = $repo->findAll();

    return $this->render('app/home.html.twig', [
      'controller_name' => 'RecipeController',
      //creer un variable pour twig afin de récuperer les recipes
      'recipes' => $recipes
    ]);
  }
}
