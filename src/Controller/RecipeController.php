<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Recipe;
use App\Repository\RecipeRepository;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


use App\Form\RecipeType;

class RecipeController extends AbstractController
{

  /**
   * @Route("/recipes", name="recipes")
   */
  public function index()
  {
    //Va demander à  doctrine de discuter avec le repo de l'entity Recipe
    //Pour chopper les recipes
    $repo = $this->getDoctrine()->getRepository(Recipe::class);

    //Puis on demande de les trouver tous
    $recipes = $repo->findAll();

    $encoders = [new JsonEncoder()]; // If no need for XmlEncoder
    $normalizers = [new ObjectNormalizer()];
    $serializer = new Serializer($normalizers, $encoders);

    // Serialize your object in Json
    $jsonObject = $serializer->serialize($recipes, 'json', [
        'circular_reference_handler' => function ($object) {
            return $object->getId();
        }
    ]);

    // For instance, return a Response with encoded Json
    $response = new Response($jsonObject, 200, ['Content-Type' => 'application/json',]);
    // $response->headers->set('Access-Control-Allow-Origin');

    return $response;


  //   $encoders = [new XmlEncoder(), new JsonEncoder()];
  //   $normalizers = [new ObjectNormalizer()];

  //   $serializer = new Serializer($normalizers, $encoders);

  //   $serializer->serialize($recipes, 'json'); 

  //   $data = [
  //       'controller_name' => 'RecipeController',
  //       //creer un variable pour twig afin de récuperer les recipes
        
  //   ];

  //   $response = new JsonResponse($data, Response::HTTP_OK);
   
  //  return $response;


    // return $this->render('recipe/index.html.twig', [
    //   'controller_name' => 'RecipeController',
    //   //creer un variable pour twig afin de récuperer les recipes
    //   'recipes' => $recipes
    // ]);
  }

  /**
   * @Route("/recipe/new", name="recipe_create")
   * @Route("/recipe/{id}/edit", name="recipe_edit")
   */
  public function form(Recipe $recipe = null, Request $request, ObjectManager $manager)
  {
    //Si je n'est pas d'article (concerne la création et pas l'edit)
    if (!$recipe) {
      $recipe = new Recipe();
    }

    //Si on utilise la commande make:form on peut faire cette commande pour récupéré le formulaire qui a été auto generer
    $form = $this->createForm(RecipeType::class, $recipe);
    //Sinon
    //creer un formulaire
    // ->add va créer les champs qu'on veut
    // peut dire que c'est un type texte TextType::class
    //peut passer des parametre pour changer la nature du inpu ou lui ajouter une classe css
    //ajouter 'class' => 'form-control' si on déclare en html nos inputs
    // $form = $this->createFormBuilder($recipe)
    //   ->add('title')
    //   ->add('text')
    //   ->add('image')
    //   ->getForm();

    //va analyser et regarder si cela à été soumis ou pas
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {

      //Si l'article n'a PAS d'id alors on met à jour la date 
      if (!$recipe->getId()) {
        $recipe->setCreatedAt(new \DateTime());
      }
      //Va faire persister la recette et balancer la requete
      $manager->persist($recipe);
      $manager->flush();

      //redirection
      return $this->redirectToRoute('recipe_show', ['id' => $recipe->getId()]);
    }

    //affichage du formulaire
    return $this->render('recipe/create.html.twig', [
      'formRecipe' => $form->createView(),
      //passe une varibale pour changer le label du bouton en cas d'edite
      'editMode' => $recipe->getId() !== null
    ]);
  }

  /**
   * @Route("/recipe/delete/{id}", name="recipe_delete")
   */

  public function delete($id)
  {
    $em = $this->getDoctrine()->getManager();
    $recipe = $em->getRepository(Recipe::class)->find($id);
    $em->remove($recipe);
    $em->flush();
    $this->addFlash('message', 'Recipe Delete OK');
    //redirection
    return $this->redirectToRoute('recipes');
  }

  // TROUVE UNE RECETTE AVEC L' ID
  /**
   * @Route("/recipe/{id}", name="recipe_show")
   */
  // public function showRecipe($id)
  public function showRecipe(Recipe $recipe)
  {
    // //on lui dit qu'on veut com,muniquer avec le repository de recipe pour récupérer ce qui se trouve dans recipe
    // $repo = $this->getDoctrine()->getRepository(Recipe::class);
    // //Puis on demande de le trouver l'id
  //  $recipe = $repo->find($id);

    $encoders = [new JsonEncoder()]; // If no need for XmlEncoder
    $normalizers = [new ObjectNormalizer()];
    $serializer = new Serializer($normalizers, $encoders);

    // Serialize your object in Json
    $jsonObject = $serializer->serialize($recipe, 'json', [
        'circular_reference_handler' => function ($object) {
            return $object->getId();
        }
    ]);

    // For instance, return a Response with encoded Json
    $response = new Response($jsonObject, 200, ['Content-Type' => 'application/json',]);
    // $response->headers->set('Access-Control-Allow-Origin');

    return $response;


    // //Puis on dit quelle varaible on veut qu'il utilise
    // return $this->render('recipe/show.html.twig', [
    //   'recipe' => $recipe
    // ]);
  }
}
