<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Recipe;
use App\Form\CommentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;

class CommentController extends AbstractController
{
  /**
   * @Route("/comment", name="comment")
   */
  public function index()
  {
    return $this->render('comment/index.html.twig', [
      'controller_name' => 'CommentController',
    ]);
  }

  /**
   * @Route("/recipe/new", name="recipe_create")
   * @Route("/recipe/{id}/edit", name="recipe_edit")
   */
  public function form(Comment $comment = null, Request $request, ObjectManager $manager, Recipe $recipe)
  {
    //Si je n'est pas d'article (concerne la création et pas l'edit)
    if (!$comment) {
      $comment = new Comment();
    }

    //Si on utilise la commande make:form on peut faire cette commande pour récupéré le formulaire qui a été auto generer
    $form = $this->createForm(CommentType::class, $comment);
    //Sinon
    //creer un formulaire
    // ->add va créer les champs qu'on veut
    // peut dire que c'est un type texte TextType::class
    //peut passer des parametre pour changer la nature du inpu ou lui ajouter une classe css
    //ajouter 'class' => 'form-control' si on déclare en html nos inputs
    // $form = $this->createFormBuilder($recipe)
    //   ->add('title')
    //   ->add('text')
    //   ->add('image')
    //   ->getForm();

    //va analyser et regarder si cela à été soumis ou pas
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {

      //Si l'article n'a PAS d'id alors on met à jour la date 
      if (!$comment->getId()) {
        $comment->setCreatedAt(new \DateTime());
      }
      //Va faire persister la recette et balancer la requete
      $manager->persist($comment);
      $manager->flush();

      //redirection
      return $this->redirectToRoute('recipe_show', ['id' => $recipe->getId()]);
    }

    //affichage du formulaire dans recipe show
    return $this->render('recipe/show.html.twig', [
      'formComment' => $form->createView(),
      //passe une varibale pour changer le label du bouton en cas d'edite
      'editMode' => $comment->getId() !== null
    ]);
  }
}
