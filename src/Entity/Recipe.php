<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RecipeRepository")
 */
class Recipe
{
  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=255)
   * @Assert\Length(min=10, max=255, minMessage="Trop court")
   */
  private $title;

  /**
   * @ORM\Column(type="text")
   * @Assert\Length(min=10)
   */
  private $text;

  /**
   * @ORM\Column(type="string", length=255)
   * @Assert\Url()
   */
  private $image;

  /**
   * @ORM\Column(type="string", length=255)
   * 
   */
  private $totalTime;

  /**
   * @ORM\Column(type="string", length=255)
   * 
   */
  private $level;

  /**
   * @ORM\Column(type="integer")
   * 
   */
  private $likes;

  /**
   * @ORM\Column(type="integer")
   * 
   */
  private $nbPerson;


  /**
   * @ORM\Column(type="datetime")
   */
  private $createdAt;

  /**
   * @ORM\Column(type="json")
   */
  private $steps;

  /**
   * @ORM\ManyToMany(targetEntity="App\Entity\Category", mappedBy="recipes")
   */
  private $categories;

  /**
   * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="recipe", orphanRemoval=true)
   */
  private $comments;

  /**
   * @ORM\ManyToMany(targetEntity="App\Entity\Ingredient", mappedBy="recipes")
   */
  private $ingredients;

  public function __construct()
  {
      $this->categories = new ArrayCollection();
      $this->comments = new ArrayCollection();
      $this->ingredients = new ArrayCollection();
  }

  public function getId(): ?int
  {
    return $this->id;
  }

  public function getTitle(): ?string
  {
    return $this->title;
  }

  public function setTitle(string $title): self
  {
    $this->title = $title;

    return $this;
  }

  

  public function getText(): ?string
  {
    return $this->text;
  }

  public function setText(string $text): self
  {
    $this->text = $text;

    return $this;
  }

  public function getImage(): ?string
  {
    return $this->image;
  }

  public function setImage(string $image): self
  {
    $this->image = $image;

    return $this;
  }

  public function getCreatedAt(): ?\DateTimeInterface
  {
    return $this->createdAt;
  }

  public function setCreatedAt(\DateTimeInterface $createdAt): self
  {
    $this->createdAt = $createdAt;

    return $this;
  }

  public function getLevel(): ?string
  {
    return $this->level;
  }

  public function setLevel(string $level): self
  {
    $this->level = $level;

    return $this;
  }

  public function getTotalTime(): ?string
  {
    return $this->totalTime;
  }

  public function setTotalTime(string $totalTime): self
  {
    $this->totalTime = $totalTime;

    return $this;
  }

  public function getLikes(): ?int
  {
    return $this->likes;
  }

  public function setLikes(int $likes): self
  {
    $this->likes = $likes;

    return $this;
  }

  public function getNbPerson(): ?int
  {
    return $this->nbPerson;
  }

  public function setNbPerson(int $nbPerson): self
  {
    $this->nbPerson = $nbPerson;

    return $this;
  }

  //Steps Array Json

  public function getSteps(): array
  {
    $steps = $this->steps;
    // guarantee every user at least has ROLE_USER
    $steps[] = 'steps';

    return array_unique($steps);
  }

  public function setSteps(array $steps): self
  {
      $this->steps = $steps;

      return $this;
  }

  /**
   * @return Collection|Category[]
   */
  public function getCategories(): Collection
  {
      return $this->categories;
  }

  public function addCategory(Category $category): self
  {
      if (!$this->categories->contains($category)) {
          $this->categories[] = $category;
          $category->addRecipe($this);
      }

      return $this;
  }

  public function removeCategory(Category $category): self
  {
      if ($this->categories->contains($category)) {
          $this->categories->removeElement($category);
          $category->removeRecipe($this);
      }

      return $this;
  }

  /**
   * @return Collection|Comment[]
   */
  public function getComments(): Collection
  {
      return $this->comments;
  }

  public function addComment(Comment $comment): self
  {
      if (!$this->comments->contains($comment)) {
          $this->comments[] = $comment;
          $comment->setRecipe($this);
      }

      return $this;
  }

  public function removeComment(Comment $comment): self
  {
      if ($this->comments->contains($comment)) {
          $this->comments->removeElement($comment);
          // set the owning side to null (unless already changed)
          if ($comment->getRecipe() === $this) {
              $comment->setRecipe(null);
          }
      }

      return $this;
  }

  /**
   * @return Collection|Ingredient[]
   */
  public function getIngredients(): Collection
  {
      return $this->ingredients;
  }

  public function addIngredient(Ingredient $ingredient): self
  {
      if (!$this->ingredients->contains($ingredient)) {
          $this->ingredients[] = $ingredient;
          $ingredient->addRecipe($this);
      }

      return $this;
  }

  public function removeIngredient(Ingredient $ingredient): self
  {
      if ($this->ingredients->contains($ingredient)) {
          $this->ingredients->removeElement($ingredient);
          $ingredient->removeRecipe($this);
      }

      return $this;
  }
}

